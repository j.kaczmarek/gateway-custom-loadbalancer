custom loadbalancer for spring-cloud-gateway
===

usage:
* run app
* go to http://localhost:8080/customergroups -> expect 500 use1-customergroups.atm.osp.tech
* go to http://localhost:8080/gwtest1        -> expect 500 use1-gwtest1.atm.osp.tech
* go to http://localhost:8080/anyOtherUrl    -> expect 404 no such route