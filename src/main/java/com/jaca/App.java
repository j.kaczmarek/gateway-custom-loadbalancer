package com.jaca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.config.LoadBalancerProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.LoadBalancerClientFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Primary @Bean
    @SuppressWarnings("unused")
    public LoadBalancerClientFilter ospFilter(RegionProvider region, LoadBalancerProperties p) {
        return new LoadBalancerClientFilter(null, p) {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                redirectToOSP(exchange);
                return chain.filter(exchange);
            }

            private void redirectToOSP(ServerWebExchange exchange) {
                URI url = exchange.getAttribute(GATEWAY_REQUEST_URL_ATTR);
                try {
                    exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR,
                            new URI("http://"+region.getRegionAlias()+"-"+url.getHost()+".atm.osp.tech"));
                } catch (URISyntaxException e) {
                    throw new IllegalStateException(e);
                }
            }
        };
    }
}
